package be.kdg.java2.exercise1_6.repository.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("prod")
public class PostgresDatabaseConfig {
    private static final Logger log = LoggerFactory.getLogger(PostgresDatabaseConfig.class);

    @Bean
    public DataSource dataSource(){
        DataSource dataSource = DataSourceBuilder
                .create()
                .driverClassName("org.postgresql.Driver")
                .url("jdbc:postgresql:personsdatabase")
                .username("postgres")
                .password("postgres")
                .build();
        return dataSource;
    }

}
