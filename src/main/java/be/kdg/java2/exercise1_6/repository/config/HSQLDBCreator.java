package be.kdg.java2.exercise1_6.repository.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Profile("dev")
public class HSQLDBCreator {
    private static final Logger log = LoggerFactory.getLogger(HSQLDBCreator.class);

    private JdbcTemplate jdbcTemplate;

    public HSQLDBCreator(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostConstruct
    public void loadData(){
        jdbcTemplate.update("DROP TABLE IF EXISTS PERSONS");
        jdbcTemplate.update("CREATE TABLE PERSONS(ID INTEGER NOT NULL IDENTITY, " +
                "NAME VARCHAR(100) NOT NULL, FIRSTNAME VARCHAR(100) NOT NULL)");
        jdbcTemplate.update("INSERT INTO PERSONS(NAME, FIRSTNAME) " +
                "VALUES ('HSQLJONES', 'JACK'), ('POTTER', 'JACK'), ('POTTER', 'MIA'), ('REED', 'JACK')");
    }

}
