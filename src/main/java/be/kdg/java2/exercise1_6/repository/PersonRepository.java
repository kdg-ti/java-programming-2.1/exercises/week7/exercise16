package be.kdg.java2.exercise1_6.repository;

import be.kdg.java2.exercise1_6.domain.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PersonRepository {
    private static final Logger log = LoggerFactory.getLogger(PersonRepository.class);

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert personInserter;

    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.personInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("PERSONS").usingGeneratedKeyColumns("id");
    }

    public List<Person> findByFirstName(String firstName){
        return jdbcTemplate.query("SELECT * FROM PERSONS WHERE FIRSTNAME = ?",
                this::mapRow, firstName);
    }

    public Person findById(int id){
        return jdbcTemplate.queryForObject("SELECT * FROM PERSONS WHERE ID = ?",
                this::mapRow, id);
    }

    public List<Person> findByName(String name){
        return jdbcTemplate.query("SELECT * FROM PERSONS WHERE NAME = ?",
                this::mapRow, name);
    }

    public Person save(Person person){
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", person.getName());
        parameters.put("FIRSTNAME", person.getFirstName());
        person.setId(personInserter.executeAndReturnKey(parameters).intValue());
        return person;
    }

    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Person(rs.getInt("id"),
                rs.getString("name"),
                rs.getString("firstname"));
    }


}
