package be.kdg.java2.exercise1_6;

import be.kdg.java2.exercise1_6.presentation.Console;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Exercise16Application {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Exercise16Application.class, args);
        context.getBean(Console.class).run();
    }

}
