package be.kdg.java2.exercise1_6.presentation;

import be.kdg.java2.exercise1_6.domain.Person;
import be.kdg.java2.exercise1_6.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Console {
    private static final Logger log = LoggerFactory.getLogger(Console.class);

    private PersonRepository personRepository;
    private Scanner scanner;

    public Console(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public void run(){
        scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Hello there, welcome to the Person Management System");
            System.out.println("What do you want to do?");
            System.out.println("1) Query by ID");
            System.out.println("2) Query by name");
            System.out.println("3) Query by firstname");
            System.out.println("4) Add a person");
            System.out.print("--> Enter your choice:");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1: queryByID();break;
                case 2: queryByName();break;
                case 3: queryByFirstname();break;
                case 4: addPerson();break;
                default:
                    System.out.println("Bad choice...");
            }
        }
    }

    private void addPerson() {
        scanner.nextLine();//to clear the buffer
        System.out.print("Please enter a name:");
        String name = scanner.nextLine();
        System.out.print("Please enter a firstname:");
        String firstname = scanner.nextLine();
        Person p = personRepository.save(new Person(name, firstname));
        System.out.println("Person saved with ID:" + p.getId());
    }

    private void queryByFirstname() {
        scanner.nextLine();//to clear the buffer
        System.out.print("Enter the firstname:");
        String firstName = scanner.nextLine();
        personRepository.findByFirstName(firstName)
                .forEach(System.out::println);
    }

    private void queryByName() {
        scanner.nextLine();//to clear the buffer
        System.out.print("Enter the name:");
        String name = scanner.nextLine();
        personRepository.findByName(name)
                .forEach(System.out::println);
    }

    private void queryByID() {
        System.out.print("Enter the ID:");
        int id = scanner.nextInt();
        System.out.println(personRepository.findById(id));
    }
}
